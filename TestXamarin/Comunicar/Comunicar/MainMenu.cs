﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Comunicar
{
    [Activity(Label = "MainMenu")]
    public class MainMenu : Activity
    {
        private Button publicEvalButton;
        private Button expertEvalButton;
        private Button reviewsButton;
        private Button aboutButton;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MainMenu);

            FindViews();
            // Create your application here
        }

        private void FindViews()
        {
            publicEvalButton = FindViewById<Button>(Resource.Id.publicEvalButton);
            expertEvalButton = FindViewById<Button>(Resource.Id.expertEvalButton);
            reviewsButton = FindViewById<Button>(Resource.Id.reviewsButton);
            aboutButton = FindViewById<Button>(Resource.Id.aboutButton);
        }   

        private void HandleEvents()
        {
            publicEvalButton.Click += PublicEvalButton_Click;
            expertEvalButton.Click += ExpertEvalButton_Click;
            reviewsButton.Click += ReviewsButton_Click;
            aboutButton.Click += AboutButton_Click;

        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void ReviewsButton_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void ExpertEvalButton_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void PublicEvalButton_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }
    }       
}           